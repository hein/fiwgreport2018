import QtQuick 2.0
import QtCharts 2.0

ChartView {
    //title: "Income vs Expenses 2020 - 2022"
    //titleFont.bold: true
    //titleFont.pointSize: 15
    legend.alignment: Qt.AlignBottom
    legend.font.pointSize: 14
    antialiasing: true
    width: 500
    height: 500

    BarSeries {
        id: mySeries
        axisY: ValueAxis {
            min: 0
            max: 600000
            labelFormat: "%d (EUR)"
        }
        axisX: BarCategoryAxis { categories: ["2019", "2020", "2021", "2022", "2023"] }
        BarSet { label: "Income"; color: "#99CA53"; values: [183882.95, 218823.87, 238900.37, 285495.97, 349332.65] }
        BarSet { label: "Expenses"; color: "#209FDF" ;values: [265678.46, 183182.93, 216563.39, 384604.78, 457071.31] }
    }
}
