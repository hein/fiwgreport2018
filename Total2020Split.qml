import QtQuick 2.0
import QtCharts 2.0

Row {
    anchors.fill: parent
    anchors.centerIn: parent

    ChartView {
        anchors.verticalCenter: parent.verticalCenter
        title: "Income"
        titleFont.bold: true
        titleFont.pointSize: 15
        legend.alignment: Qt.AlignRight
        legend.font.pointSize: 12
        theme: ChartView.ChartThemeQt
        antialiasing: true
        width: parent.width / 2
        height: parent.height / 1.5
        margins.left: 0
        margins.right: 0
        margins.bottom: 0
        margins.top: 0
        
        PieSeries {
            size: 0.9
            PieSlice { color: "#e6261f"; label: "Patrons"; value: 43900.00 }
            PieSlice { color: "#eb7532"; label: "Supporting members<br>and donations"; value: 140791.44 }
            PieSlice { color: "#f7d038"; label: "Akademy"; value: 24430.00 }
            //PieSlice { color: "#a3e048"; label: "Other events"; value: 8262.66 }
            PieSlice { color: "#49da9a"; label: "GSoC and Code-In"; value: 9487.09 }
            //PieSlice { color: "#34bbe6"; label: "Other"; value: 215.34 }
        }
    }

    ChartView {
        anchors.verticalCenter: parent.verticalCenter
        title: "Expenses"
        titleFont.bold: true
        titleFont.pointSize: 15
        legend.alignment: Qt.AlignRight
        legend.font.pointSize: 14
        theme: ChartView.ChartThemeQt
        antialiasing: true
        width: parent.width / 2
        height: parent.height / 1.5
        margins.left: 0
        margins.right: 0
        margins.bottom: 0
        margins.top: 0

        PieSeries {
            size: 0.9
            PieSlice { color: "#e6261f"; label: "Staff & contractors"; value: 112206.47 }
            PieSlice { color: "#eb7532"; label: "Akademy"; value: 6847.40 }
            PieSlice { color: "#f7d038"; label: "Sprints  "; value: 6304.04 }
            PieSlice { color: "#a3e048"; label: "Other events"; value: 9737.30 }
            PieSlice { color: "#49da9a"; label: "Infrastructure"; value: 5810.05 }
            PieSlice { color: "#34bbe6"; label: "Office"; value: 8791.35 }
            PieSlice { color: "#4355db"; label: "Taxes & Insurances"; value: 22183.03 }
            PieSlice { color: "#d23be7"; label: "Other"; value: 7370.41 }
        }
    }
}
