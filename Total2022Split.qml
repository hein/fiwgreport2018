import QtQuick 2.0
import QtCharts 2.0

Row {
    anchors.fill: parent
    anchors.centerIn: parent

    ChartView {
        anchors.verticalCenter: parent.verticalCenter
        title: "Income"
        titleFont.bold: true
        titleFont.pointSize: 15
        legend.alignment: Qt.AlignRight
        legend.font.pointSize: 12
        theme: ChartView.ChartThemeQt
        antialiasing: true
        width: parent.width / 2
        height: parent.height / 1.5
        margins.left: 0
        margins.right: 0
        margins.bottom: 0
        margins.top: 0
        
        PieSeries {
            size: 0.9
            PieSlice { color: "#e6261f"; label: "Patrons"; value: 67,000.00 }
            PieSlice { color: "#eb7532"; label: "Supporting members<br>and donations"; value: 121,577.43 }
            PieSlice { color: "#f7d038"; label: "Akademy"; value: 40,225.75 }
            PieSlice { color: "#a3e048"; label: "Other events"; value: 11,927.50 }
            PieSlice { color: "#49da9a"; label: "GSoC and Code-In"; value: 4,496.43 }
            PieSlice { color: "#34bbe6"; label: "Other"; value: 40,268.86 }
        }
    }

    ChartView {
        anchors.verticalCenter: parent.verticalCenter
        title: "Expenses"
        titleFont.bold: true
        titleFont.pointSize: 15
        legend.alignment: Qt.AlignRight
        legend.font.pointSize: 14
        theme: ChartView.ChartThemeQt
        antialiasing: true
        width: parent.width / 2
        height: parent.height / 1.5
        margins.left: 0
        margins.right: 0
        margins.bottom: 0
        margins.top: 0

        PieSeries {
            size: 0.9
            PieSlice { color: "#e6261f"; label: "Staff & contractors"; value: 209,465.82 }
            PieSlice { color: "#eb7532"; label: "Akademy"; value: 68,774.97 }
            PieSlice { color: "#f7d038"; label: "Sprints  "; value: 14,071.52 }
            PieSlice { color: "#a3e048"; label: "Other events"; value: 14,275.11 }
            PieSlice { color: "#49da9a"; label: "Infrastructure"; value: 12,202.23 }
            PieSlice { color: "#34bbe6"; label: "Office"; value: 8,233.56 }
            PieSlice { color: "#4355db"; label: "Taxes & Insurances"; value: 26,569.41 }
            PieSlice { color: "#d23be7"; label: "Other"; value: 31,012.16 }
        }
    }
}
