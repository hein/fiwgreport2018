import QtQuick 2.0
import QtCharts 2.0

Row {
    anchors.fill: parent
    anchors.centerIn: parent

    ChartView {
        anchors.verticalCenter: parent.verticalCenter
        title: "Income"
        titleFont.bold: true
        titleFont.pointSize: 15
        legend.alignment: Qt.AlignRight
        legend.font.pointSize: 12
        theme: ChartView.ChartThemeQt
        antialiasing: true
        width: parent.width / 2
        height: parent.height / 1.5
        margins.left: 0
        margins.right: 0
        margins.bottom: 0
        margins.top: 0
        
        PieSeries {
            size: 0.9
            PieSlice { color: "#e6261f"; label: "Patrons"; value: 64700.00 }
            PieSlice { color: "#eb7532"; label: "Supporting members<br>and donations"; value: 104713.28 }
            PieSlice { color: "#f7d038"; label: "Akademy"; value: 29840.55 }
            PieSlice { color: "#a3e048"; label: "Other events"; value: 14199.48 }
            PieSlice { color: "#49da9a"; label: "GSoC and Code-In"; value: 5296.08 }
            PieSlice { color: "#34bbe6"; label: "Other"; value: 20180.28 }
        }
    }

    ChartView {
        anchors.verticalCenter: parent.verticalCenter
        title: "Expenses"
        titleFont.bold: true
        titleFont.pointSize: 15
        legend.alignment: Qt.AlignRight
        legend.font.pointSize: 14
        theme: ChartView.ChartThemeQt
        antialiasing: true
        width: parent.width / 2
        height: parent.height / 1.5
        margins.left: 0
        margins.right: 0
        margins.bottom: 0
        margins.top: 0

        PieSeries {
            size: 0.9
            PieSlice { color: "#e6261f"; label: "Staff & contractors"; value: 150289.93 }
            PieSlice { color: "#eb7532"; label: "Akademy"; value: 3609.10 }
            PieSlice { color: "#f7d038"; label: "Sprints  "; value: 0.00 }
            PieSlice { color: "#a3e048"; label: "Other events"; value: 666.35 }
            PieSlice { color: "#49da9a"; label: "Infrastructure"; value: 9046.76 }
            PieSlice { color: "#34bbe6"; label: "Office"; value: 7079.03 }
            PieSlice { color: "#4355db"; label: "Taxes & Insurances"; value: 21038.50 }
            PieSlice { color: "#d23be7"; label: "Other"; value: 26667.08 }
        }
    }
}
