import QtQuick 2.0
import QtCharts 2.0

ChartView {
    title: "Income: 349,332.65 EUR in 2023"
    titleFont.bold: true
    titleFont.pointSize: 15
    legend.alignment: Qt.AlignBottom
    legend.font.pointSize: 14
    antialiasing: true
    width: 500
    height: 500

    StackedBarSeries {
        id: mySeries
        axisX: BarCategoryAxis { categories: ["2019", "2020*", "2021", "2022", "2023"] }
        axisY: ValueAxis {
            min: 0
            max: 600000
            labelFormat: "%d (EUR)"
        }
        BarSet { label: "Income (minus Akademy)"; values: [132238.81, 115312.31, 184640.79, 245270.22, 291582.06] }
        BarSet { label: "Akademy"; values: [51644.14, 24430.00, 29840.55, 40225.75, 57750.05] }
        BarSet { color: "#289c45"; label: "Large one-time donations"; values: [0, 79081.56, 24448.33, 0, 0] }
    }

    Text {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -(parent.height * 0.09)
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width * 0.85
        font.family: "Noto Sans"
        font.pointSize: 11
        wrapMode: Text.Wrap
        text: "* = An unusually high amount of 23.500 EUR of corporate membership fees invoiced in 2020 was received at a delay in 2021. Taken into account, income excluding event sponsoring and large one-time donations (blue) increased slightly in 2020. Also received in 2021 was sponsorship income for Linux App Summit (LAS) 2020."
    }
}
