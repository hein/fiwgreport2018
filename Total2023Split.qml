import QtQuick 2.0
import QtCharts 2.0

Row {
    anchors.fill: parent
    anchors.centerIn: parent

    ChartView {
        anchors.verticalCenter: parent.verticalCenter
        title: "Income"
        titleFont.bold: true
        titleFont.pointSize: 15
        legend.alignment: Qt.AlignRight
        legend.font.pointSize: 12
        theme: ChartView.ChartThemeQt
        antialiasing: true
        width: parent.width / 2
        height: parent.height / 1.5
        margins.left: 0
        margins.right: 0
        margins.bottom: 0
        margins.top: 0

        PieSeries {
            size: 0.9
            PieSlice { color: "#e6261f"; label: "Patrons"; value: 80219.74 }
            PieSlice { color: "#eb7532"; label: "Supporting members<br>and donations"; value: 181564.20 }
            PieSlice { color: "#f7d038"; label: "Akademy"; value: 57750.05 }
            PieSlice { color: "#a3e048"; label: "Other events"; value: 9030.83 }
            PieSlice { color: "#49da9a"; label: "GSoC and Code-In"; value: 5673.29 }
            PieSlice { color: "#34bbe6"; label: "Other"; value: 15094.55 }
        }
    }

    ChartView {
        anchors.verticalCenter: parent.verticalCenter
        title: "Expenses"
        titleFont.bold: true
        titleFont.pointSize: 15
        legend.alignment: Qt.AlignRight
        legend.font.pointSize: 14
        theme: ChartView.ChartThemeQt
        antialiasing: true
        width: parent.width / 2
        height: parent.height / 1.5
        margins.left: 0
        margins.right: 0
        margins.bottom: 0
        margins.top: 0

        PieSeries {
            size: 0.9
            PieSlice { color: "#e6261f"; label: "Staff & contractors"; value: 317263.14 }
            PieSlice { color: "#eb7532"; label: "Akademy"; value: 43129.19 }
            PieSlice { color: "#f7d038"; label: "Sprints  "; value: 12883.29 }
            PieSlice { color: "#a3e048"; label: "Other events"; value: 20549.46 }
            PieSlice { color: "#49da9a"; label: "Infrastructure"; value: 17778.95 }
            PieSlice { color: "#34bbe6"; label: "Office"; value: 7241.61 }
            PieSlice { color: "#4355db"; label: "Taxes & Insurances"; value: 22614.56 }
            PieSlice { color: "#d23be7"; label: "Other"; value: 15611.11 }
        }
    }
}
