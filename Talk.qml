/***************************************************************************
 *   Copyright (C) 2017 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

import "presentation"
import QtQuick 2.6

Presentation
{
    id: presentation

    width: square ? 1024 : 1280
    height: square ? 768 : 720

    property bool square: false

    property variant fromSlide: null
    property variant toSlide: null

    property int transitionTime: 0

    fontFamily: "Noto Sans"

    showNotes: false

    Image {
        id: transitionLogo

        visible: false

        width: Math.min(parent.width/2, parent.height/2)
        height: width

        opacity: 0.0

        z: 9999

        anchors.centerIn: parent

        smooth: true
        source: "kdeev.svg"
        sourceSize.width: width
        sourceSize.height: height
    }

    Image {
        opacity: (presentation.currentSlide > 1) ? 1.0 : 0.0

        Behavior on opacity {
            NumberAnimation { duration: presentation.transitionTime/2; easing.type: Easing.InQuart }
        }

        width: (slideCounter.font.pixelSize * 3.2)
        height: width/2.022

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: slideCounter.font.pixelSize

        smooth: true
        source: "kdeev.svg"
        sourceSize.width: width
        sourceSize.height: height
    }

    SequentialAnimation {
        id: forwardTransition
        alwaysRunToEnd: true

        PropertyAction { target: toSlide; property: "visible"; value: true }
        PropertyAction { target: transitionLogo; property: "visible"; value: ((presentation.currentSlide == 0) ? true : false) }
        ParallelAnimation {
            NumberAnimation { target: fromSlide; property: "opacity"; from: 1; to: 0; duration: presentation.transitionTime; easing.type: Easing.OutQuart }
            NumberAnimation { target: fromSlide; property: "scale"; from: 1; to: 1.1; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
            NumberAnimation { target: toSlide; property: "opacity"; from: 0; to: 1; duration: presentation.transitionTime; easing.type: Easing.InQuart }
            NumberAnimation { target: toSlide; property: "scale"; from: 0.7; to: 1; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
            NumberAnimation { target: transitionLogo; property: "opacity"; from: 0.4; to: 0.0; duration: presentation.transitionTime; easing.type: Easing.OutQuart }
            NumberAnimation { target: transitionLogo; property: "scale"; from: 0.8; to: 4; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
        }
        PropertyAction { target: transitionLogo; property: "visible"; value: false }
        PropertyAction { target: fromSlide; property: "visible"; value: false }
        PropertyAction { target: fromSlide; property: "scale"; value: 1 }
    }

    SequentialAnimation {
        id: backwardTransition
        alwaysRunToEnd: true

        PropertyAction { target: toSlide; property: "visible"; value: true }
        ParallelAnimation {
            NumberAnimation { target: fromSlide; property: "opacity"; from: 1; to: 0; duration: presentation.transitionTime; easing.type: Easing.OutQuart }
            NumberAnimation { target: fromSlide; property: "scale"; from: 1; to: 0.7; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
            NumberAnimation { target: toSlide; property: "opacity"; from: 0; to: 1; duration: presentation.transitionTime; easing.type: Easing.InQuart }
            NumberAnimation { target: toSlide; property: "scale"; from: 1.1; to: 1; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
        }
        PropertyAction { target: fromSlide; property: "visible"; value: false }
        PropertyAction { target: fromSlide; property: "scale"; value: 1 }
    }

    function switchSlides(from, to, forward)
    {
        if (forwardTransition.running || backwardTransition.running) {
            return false;
        }

        presentation.fromSlide = from;
        presentation.toSlide = to;

        if (forward) {
            forwardTransition.start();
        } else {
            backwardTransition.start();
        }

        return true;
    }

    SlideCounter { id: slideCounter }

    Slide {
        fontFamily: "Noto Sans"

        Image {
            width: parent.height/1.6
            height: width/2.022

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: -(parent.height/12)

            verticalAlignment: Image.AlignTop

            smooth: true
            source: "kdeev.svg"
            sourceSize.width: width
            sourceSize.height: height
        }

        centeredText: "<h1></h1><h1></h1><h1></h1>\n<h2><b>Financial Working Group</b></h2><h5>— Akademy Report 2024 —</h5>\n<h5>August 2024</h5>"
    }

    Slide {
        title: "Mission & Goals"

        fontScale: 0.70

        content: [
            "Support KDE e.V. Board in Financial topics.",
            "In collaboration with the Treasurer, propose financial decisions to the KDE e.V. Board.",
            "Support Board members in commercial activities.",
            "Support the Treasurer in communication actions related to financial topics.",
            "Evaluate financial reports submitted by the Treasurer.",
            "Help the Community implement the annual Budget Plan.",
        ]
    }

    Slide {
        title: "Members: Elected for two-year terms"

        fontScale: 0.60
        textFormat: Text.RichText

        content: [
            "Eike Hein (hein@kde.org)<br /><font color=\"green\">Active.</font> Treasurer.",
            "Marta Rybczynska (marta.rybczynska@kde.org)<br /><font color=\"green\">Active.</font> (elected June 2023).",
            "Till Adam (adam@kde.org)<br /><font color=\"green\">Active.</font> (elected June 2023)."
        ]
    }

    Slide {
        title: "2023 Financial Review"

        fontScale: 0.72

        content: [
            "Overall a stable year with no negative surprises.",
            "Income increased from 285k to 350k, thanks to a lot of new fundraising.",
            " The highlight is our rejuvenated Supporting Membership program on Donorbox.",
            "Expenses increased from 385k to 457k due to post-pandemic recovery and full-year hiring.",
            " The bulk of our expenses continue to be personnel expenses.",
            "Akademy 2023 set a new sponsorship record, but this was largely offset by organization expenses.",
            " The times of earning through Akademy may be behind us.",
            "Planning performance: Income within 500 EUR of projection; expenses below target, but in line with expectations.",
            " As planned, we've outspent our income for the second year in a row to reduce our reserves."
        ]
    }

    Slide {
        title: "Income 2018-2023"
        TotalIn {
            anchors.fill: parent
        }
    }

    Slide {
        title: "Expenses 2018-2023"
        TotalOut {
            anchors.fill: parent
        }
    }

    Slide {
        title: "Income vs. Expenses 2019-2023"
        TotalInOutRatio {
            anchors.fill: parent
        }
    }

    Slide {
        title: "2023 in Detail"
        Total2023Split {
            anchors.fill: parent
        }
    }

    Slide {
        title: "2024 Budget Plan"

        fontScale: 0.72

        content: [
          "Done in collaboration.",
          " Drafted by the Treasurer with input from the KDE e.V. board & other stakeholders.",
          " Reviewed and improved with feedback by the Financial Working Group.",
          "We expect to grow the income to 475k, on the back of additional fundraising and the new NS4NH grant.",
          "We've taken steps to limit expense growth. Our goal is a reserve burn rate reduction of 18% in 2024.",
          "Longer term, we want to balance the budget (break even) in the 2026-2027 timeframe.",
          " Fundraising has to stay strong for this to work. If it doesn't, we will take steps to reign in costs."
        ]
    }

    Slide {
        title: "2024 Mid-Year Review & Future Outlook"

        fontScale: 0.72

        content: [
            "Generally things are on track.",
            "We've had a conf.kde.in event again!",
            "Akademy sponsorship came in lower than 2023. It's a tough year for conferences.",
            "The KDE League dissolution has finally passed.",
            "Plasma 6 does not happen every year, so we may need to push on fundraising toward Q4."
        ]
    }

    Slide {
        title: "FiWG Tools"

        fontScale: 0.72

        content: [
            "Our finance dashboard, Treasure Chest, has proven an extremely valuable tool.",
            "In 2024, it gained new features to track and project the performance our fundraising campaigns.",
            " Can track campaigns across different channels.",
            " Better accounting of fees & actuals than Donorbox' own tools.",
            " Capability used in 2024 budget planning."
        ]
    }
}
