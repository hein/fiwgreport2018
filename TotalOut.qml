import QtQuick 2.0
import QtCharts 2.0

ChartView {
    title: "Expenses: 457,071.31 EUR in 2023"
    titleFont.bold: true
    titleFont.pointSize: 15
    legend.alignment: Qt.AlignBottom
    legend.font.pointSize: 14
    antialiasing: true
    width: 500
    height: 500

    StackedBarSeries {
        id: mySeries
        axisY: ValueAxis {
            min: 0
            max: 600000
            labelFormat: "%d (EUR)"
        }
        axisX: BarCategoryAxis { categories: ["2019", "2020", "2021", "2022", "2023"] }
        BarSet { label: "Expenses (minus Akademy)"; values: [188182.75, 172402.65, 214787.65, 315829.81, 413942.12] }
        BarSet { label: "Akademy*"; values: [6847.40, 3609.10, 40225.75, 68774.97, 43129.19] }
    }

    Text {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -(parent.height * 0.09)
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width * 0.85
        font.family: "Noto Sans"
        font.pointSize: 11
        wrapMode: Text.Wrap
        text: "* = Akademy expenses do not include the compensation of our Event Organizer, who works on multiple events. It is is accounted in our personnel expenses instead."
    }
}
